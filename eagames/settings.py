# -*- coding: utf-8 -*-
"""
Django settings for eagames project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import sys
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

sys.path.append( os.path.join(BASE_DIR, 'app'))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '7hfb!%pn2ypuouz1-wccvc_^*rg$@7$^o=)bz57-i4n!8o9u#w'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

AUTH_USER_MODEL = 'accounts.UserProfile'

ALLOWED_HOSTS = ['109.234.34.22']


# Application definition

INSTALLED_APPS = (
    'grappelli',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'endless_pagination',
    'south',
    'registration',
    'social_auth',
    'easy_thumbnails',
    'bootstrap3',
    'accounts',
    'core',
    'payment',
    'chat',
    'arcticles',
    'notification',
    'djangorpc',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    #'django.middleware.csrf.CsrfViewMiddleware',
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'django.contrib.staticfiles.finders.FileSystemFinder',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.core.context_processors.request",
    "django.contrib.messages.context_processors.messages",
     'social_auth.context_processors.social_auth_by_name_backends',
    'social_auth.context_processors.social_auth_backends',
    'social_auth.context_processors.social_auth_by_type_backends',
    'social_auth.context_processors.social_auth_login_redirect',
)

ROOT_URLCONF = 'eagames.urls'

WSGI_APPLICATION = 'eagames.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'eagames',
        'USER': 'root',
        #'PASSWORD': 'кошуки18',
        'PASSWORD': 'EAGAMES',
        'HOST': 'localhost',
        'PORT': '3306',
    },

    'users': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'eausers',
        'USER': 'root',
        'PASSWORD': 'кошуки18',
        'HOST': 'localhost',
        'PORT': '3306',
    }
}
# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'ru'

TIME_ZONE = "Europe/Moscow"

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/
ROOT_PATH = os.path.abspath(os.path.dirname(__file__))

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

MEDIA_URL = '/media/'

STATIC_URL = '/static/'

STATIC_ROOT = os.path.join(ROOT_PATH, 'static')

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR,  'templates'),
)

AUTHENTICATION_BACKENDS = (
    'social_auth.backends.twitter.TwitterBackend',
    'social_auth.backends.facebook.FacebookBackend',
    'social_auth.backends.google.GoogleOAuthBackend',
    'social_auth.backends.google.GoogleOAuth2Backend',
    'social_auth.backends.google.GoogleBackend',
    'social_auth.backends.yahoo.YahooBackend',
    'social_auth.backends.browserid.BrowserIDBackend',
    'social_auth.backends.contrib.linkedin.LinkedinBackend',
    'social_auth.backends.contrib.disqus.DisqusBackend',
    'social_auth.backends.contrib.livejournal.LiveJournalBackend',
    'social_auth.backends.contrib.orkut.OrkutBackend',
    'social_auth.backends.contrib.foursquare.FoursquareBackend',
    'social_auth.backends.contrib.github.GithubBackend',
    'social_auth.backends.contrib.vk.VKOAuth2Backend',
    'social_auth.backends.contrib.live.LiveBackend',
    'social_auth.backends.contrib.skyrock.SkyrockBackend',
    'social_auth.backends.contrib.yahoo.YahooOAuthBackend',
    'social_auth.backends.contrib.readability.ReadabilityBackend',
    'social_auth.backends.contrib.fedora.FedoraBackend',
    'social_auth.backends.OpenIDBackend',
    'django.contrib.auth.backends.ModelBackend',
)

VK_APP_ID = '4732752'
VKONTAKTE_APP_ID = VK_APP_ID
VK_API_SECRET = 'AlKkfiAKxJJHSiUqV3n3'
VKONTAKTE_APP_SECRET = VK_API_SECRET

SOCIAL_AUTH_NEW_USER_REDIRECT_URL = '/faq/'
"""
DATABASE_ROUTERS = ['eagames.router.MasterSlaveRouter',]
"""

SESSION_ENGINE = 'redis_sessions.session'

CHAT_API_KEY = 'o9d4fa2$ue4y=afajp$0gp&m^kp'

SEND_MESSAGE_API_URL = 'http://raiserate.com/apichat/api/router/'

THUMBNAIL_ALIASES = {
    '': {
        'avatar': {'size': (50, 50), 'crop': True},
    },
}

ACCOUNT_ACTIVATION_DAYS = 2 

EMAIL_HOST = 'smtp.mandrillapp.com'
EMAIL_HOST_USER = 'andron.andr@gmail.com'
EMAIL_HOST_PASSWORD = 'ed68ff9NVDRps1RE3cuLaA'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
SERVER_EMAIL = EMAIL_HOST_USER
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER

ID_MAGAZINE = '8502'
ID_SECRET_KEY = 'raitetogoooo'

MIN_SUMMA = 50
MAX_SUMMA = 2000

