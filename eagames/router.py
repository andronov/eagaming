class MasterSlaveRouter(object):
    def db_for_read(self, model, **hints):
        if model._meta.db_table == 'accounts':
            return 'users'
        return 'default'
