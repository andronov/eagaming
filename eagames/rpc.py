from chat.rpc import ChatApiClass
from djangorpc import RpcRouter


class CustomRouter(RpcRouter):

    def extra_kwargs(self, request, *args, **kwargs):
        return {
            'request': request,
        }


api_router = CustomRouter({
    'ChatApi': ChatApiClass(),
})