from registration.forms import RegistrationFormUniqueEmail
from registration.views import RegistrationView
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.staticfiles import views
from django.contrib.auth.views import login, logout
from django.conf import settings
#from rpc import api_router
from accounts.forms import CustomRegistrationForm, CustomRegistrationFormUniqueEmail
from accounts.views import SettingsView, PaymentSettingsView, SecuritySettingsView, RatesView, HistoryExpressView, \
    ReferalView
from core.views import HomePageView, LineBetsView, DetailBetsView, sert, ExpressView, TestView
from eagames.rpc import api_router
from payment.views import PaymentView, CashoutView, payment

admin.autodiscover()



urlpatterns = patterns('',

    url(r'^$', HomePageView.as_view(), name='home'),
    url(r'^1EDEF48E2BFBCD73E1C4E6C164816E3D.txt', sert, name='sert'),
    url(r'^4007d082e58e.html', TestView.as_view(), name='html'),
    url(r'^table/$', LineBetsView.as_view(), name='linetable'),
    url(r'^table/many/$', 'core.views.linetable_many', name='linetable-many'),
    url(r'^express/$', ExpressView.as_view(), name='express'),
    url(r'^table/(?P<slug>[-\w]+)/$', DetailBetsView.as_view(), name='detailline'),

    url(r'^rates/$', RatesView.as_view(), name='rates'),
    url(r'^express/history$', HistoryExpressView.as_view(), name='express-history'),

    url(r'^referal/$', ReferalView.as_view(), name='referal'),

    url(r'^test', 'chat.views.test', name='test'),
    url(r'^apichat/upload$', 'chat.views.upload', name='upload'),
    url(r'^apichat/api/', include(api_router.urls)),
    
    #payment
    url(r'^payment/$', PaymentView.as_view(), name='payment-pay'),
    url(r'^payment/get$', payment, name='payment-get'),
    url(r'^cashout/$', CashoutView.as_view(), name='cashout'),

    # accounts - base actions
    (r'^accounts/login/$',  login),
    (r'^accounts/logout/$', logout, {'next_page': '/'}),

    #accounts pages
    url(r'^accounts/settings/$', SettingsView.as_view(), name='settings'),
    url(r'^accounts/settings/payment/$', PaymentSettingsView.as_view(), name='payment-settings'),
    url(r'^accounts/settings/security/$', SecuritySettingsView.as_view(), name='security-settings'),

    # accounts - integrate with third apps
    url(r'^accounts/register/$', 'accounts.views.registration',
            name='registration'),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^social/', include('social_auth.urls')),

    (r'^grappelli/', include('grappelli.urls')), # grappelli URLS
    url(r'^admin/', include(admin.site.urls)),

    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
       'document_root': settings.MEDIA_ROOT}),

    # articles
    url('', include('arcticles.urls')),


)
"""
    url(r'^accounts/register/$', 'accounts.views.registration',
            name='registration'),
    url(r'^accounts/password/change/$', auth_views.password_change,
            name='password_change'),
    url(r'^accounts/password/change/done/$', auth_views.password_change_done,
            name='password_change_done'),
    url(r'^accounts/password/reset/$', auth_views.password_reset,
            {'password_reset_form': MyPasswordResetForm}, name='password_reset'),
    url(r'^accounts/password/reset/done/$',
            auth_views.password_reset_done, name='password_reset_done'),
    url(r'^accounts/password/reset/complete/$',
            auth_views.password_reset_complete, name='password_reset_complete'),
    url(r'^accounts/password/reset/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
            auth_views.password_reset_confirm, name='password_reset_confirm'),
"""


urlpatterns += [
        url(r'^static/(?P<path>.*)$', views.serve),
]