import datetime

from django import template

register = template.Library()

@register.filter
def plus_days(value, days):
    print(datetime.timedelta(days=days))
    #return value + datetime.timedelta(days=days)

@register.filter
def plus_count(value):
    return int(value)+1