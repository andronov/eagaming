# -*- coding: utf-8 -*-
from django.contrib import admin

from core.models import *

# Register your models here.

class LineBetsAdmin(admin.ModelAdmin):
    model = LineBets

    prepopulated_fields = {"slug": ('slug',)}
    list_display = ['team_discipline_name', 'team_turnir_name', 'team_one_name', 'team_two_name', 'team_one_ration',
                    'team_two_ration', 'is_active', 'is_finish', 'is_cancel']
    #search_fields = ('team_one_name', )

    def team_one_name(self, instance):
        return instance.team_one.name
    def team_two_name(self, instance):
        return instance.team_two.name
    def team_discipline_name(self, instance):
        return instance.discipline.name
    def team_turnir_name(self, instance):
        return instance.turnir.name

    team_discipline_name.short_description = 'Дисциплина'
    team_turnir_name.short_description = 'Турнир'
    team_one_name.short_description = 'Первая команда'
    team_two_name.short_description = 'Вторая команда'


admin.site.register(Discipline)
admin.site.register(Turnir)
admin.site.register(Team)
admin.site.register(Country)
admin.site.register(Event)
admin.site.register(LineBets, LineBetsAdmin)
admin.site.register(BetEvent)
admin.site.register(Bet)
admin.site.register(BetExpress)