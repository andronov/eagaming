# -*- coding: utf-8 -*-
import json
import datetime
import mimetypes
import os
from django.conf import settings
from django.core.context_processors import csrf
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from django.template import RequestContext
from django.template.loader import render_to_string
from django.views.generic import TemplateView, ListView, DetailView
from django.contrib import messages
from accounts import UserProfile
from accounts.models import Referal
from core.models import *


class HomePageView(TemplateView):
    template_name = 'core/home.html'

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        context.update(csrf(self.request))
        context['discipline'] = Discipline.objects.filter(is_active=True)
        return context


    def render_to_response(self, context, **response_kwargs):
        response = super(HomePageView, self).render_to_response(context, **response_kwargs)

        if 'pl' in self.request.GET:
            response.set_cookie("ref", self.request.GET.get('pl'))
            if Referal.objects.get(user_id=self.request.GET.get('pl')):
                ref = Referal.objects.get(user_id=self.request.GET.get('pl'))
                ref.transition += 1
                ref.save()

        return response

class LineBetsView(ListView):
    queryset = LineBets.objects.filter(is_active=True).order_by('date_start')
    template_name = 'core/linetable_list.html'

    def get_queryset(self):
        qs = super(LineBetsView, self).get_queryset()
        return qs

    def render_to_response(self, context, **response_kwargs):
        response = super(LineBetsView, self).render_to_response(context, **response_kwargs)
        if self.disc:
            response.set_cookie("discip", self.disc)
        if self.times == '':
            response.set_cookie("time", 'all')
            if not self.disc:
                response.set_cookie("discip", [t.id for t in Discipline.objects.filter(is_active=True)])
        return response

    def get_context_data(self, **kwargs):
        context = super(LineBetsView, self).get_context_data(**kwargs)

        if self.request.COOKIES.get('time'):
            self.times = self.request.COOKIES.get('time')
        else:
            self.times = ''

        self.disc = ''
        if 'disc' in self.request.GET:
            self.disc = self.request.GET['disc']
        if self.request.COOKIES.get('discip'):
            print(self.request.COOKIES.get('discip'))
            if self.request.COOKIES.get('discip') == [0]:
                self.discip = [0]
            else:
               self.discip = list(self.request.COOKIES.get('discip').replace('[', '').replace(',', '').replace(']', '').replace(' ', ''))
        else:
            self.discip = [t.id for t in Discipline.objects.filter(is_active=True)]

        if self.disc:
            self.discip = self.disc

        if self.times == '' or self.times == 'all':
            self.queryset = self.queryset.filter(discipline__id__in=self.discip, is_active=True)
        else:
            d2 = datetime.datetime.strptime(self.times,'%Y,%m,%d')
            self.queryset = self.queryset.filter(discipline__id__in=self.discip, is_active=True, date_start__startswith=datetime.date(int(d2.year),int(d2.month),int(d2.day)))

        self.queryset = self.queryset.filter(discipline__id__in=self.discip, is_active=True)
        context['linebets'] = self.queryset
        context['discipline'] = Discipline.objects.filter(is_active=True)
        return context

    def post(self, request, *args, **kwargs ):

        desc_id = []
        desc_post = request.POST.getlist('discip[]')
        print(desc_id, desc_post)
        times = self.request.COOKIES.get('time')
        for s in desc_post:
            desc_id.append(int(s))
        if not desc_id:
           self.object_list = self.get_queryset().filter(discipline__id=0)
        else:
           self.object_list = self.get_queryset().filter(discipline__id__in=desc_id)


        if 'time' in request.POST and 'discip[]' in request.POST:
            if request.POST['time'] == 'all':
                times = 'all'
                self.object_list = self.get_queryset().filter(discipline__id__in=desc_id)
            else:
                times = request.POST['time']
                d2 = datetime.datetime.strptime(request.POST['time'],'%Y,%m,%d')
                self.object_list = self.get_queryset().filter(discipline__id__in=desc_id, date_start__startswith=datetime.date(int(d2.year),int(d2.month),int(d2.day)))
        elif 'time' in request.POST:
            if request.POST['time'] == 'all':
                times = 'all'
                if desc_id:
                    self.object_list = self.get_queryset().filter(discipline__id__in=desc_id)
                else:
                    self.object_list = self.get_queryset()
            else:
                times = request.POST['time']
                if desc_id:
                    d2 = datetime.datetime.strptime(request.POST['time'],'%Y,%m,%d')
                    self.object_list = self.get_queryset().filter(discipline__id__in=desc_id,date_start__startswith=datetime.date(int(d2.year),int(d2.month),int(d2.day)))
                else:
                    d2 = datetime.datetime.strptime(request.POST['time'],'%Y,%m,%d')
                    self.object_list = self.get_queryset().filter(date_start__startswith=datetime.date(int(d2.year),int(d2.month),int(d2.day)))
        elif 'discip[]' in request.POST:
                times = self.request.COOKIES.get('time')
                if times == 'all':
                    self.object_list = self.get_queryset().filter(discipline__id__in=desc_id)
                else:
                   d2 = datetime.datetime.strptime(times,'%Y,%m,%d')
                   self.object_list = self.get_queryset().filter(discipline__id__in=desc_id, date_start__startswith=datetime.date(int(d2.year),int(d2.month),int(d2.day)))
        if not desc_id:
            self.object_list = self.get_queryset().filter(discipline__id__in=[0])


        context = self.get_context_data()
        data = {}
        data['html'] = render_to_string("core/table/ajax.html", context)
        response = HttpResponse(json.dumps({'data':data}), content_type="application/json")
        print(777, times)
        if times:
           response.set_cookie("time", times)
        if desc_id:
           response.set_cookie("discip", desc_id)
        else:
            response.set_cookie("discip", [0])
        return response



class DetailBetsView(DetailView):

    model = LineBets

    def get_context_data(self, **kwargs):
        context = super(DetailBetsView, self).get_context_data(**kwargs)
        return context

    def post(self, request, *args, **kwargs ):
        context = RequestContext(request)
        data = {}
        print(request.POST)

        #Проверка суммы
        amount = ''
        if request.POST.get('osn-1') != '':
            amount = request.POST.get('osn-1')
        elif request.POST.get('osn-2') != '':
            amount = request.POST.get('osn-2')
        if int(amount) > int(request.user.amount):
            data['error'] = 'Недостаточно средств'.decode("utf8")
            response = HttpResponse(json.dumps({'data':data}), content_type="application/json")
            return response
        elif int(amount) < int(settings.MIN_SUMMA):
            data['error'] = 'Сумма для ставки должна быть больше 50 руб.'.decode("utf8")
            response = HttpResponse(json.dumps({'data':data}), content_type="application/json")
            return response
        elif int(amount) > int(settings.MAX_SUMMA):
            data['error'] = 'Сумма для ставки превышает 1000 руб.'.decode("utf8")
            response = HttpResponse(json.dumps({'data':data}), content_type="application/json")
            return response

        #Проверка активно ли событие
        linebets = LineBets.objects.get(id=request.POST.get('line'))
        if linebets.is_finish:
            data['error'] = 'Событие завершено'
            response = HttpResponse(json.dumps({'data':data}), content_type="application/json")
            return response
        elif linebets.is_cancel:
            data['error'] = 'Событие отменено'
            response = HttpResponse(json.dumps({'data':data}), content_type="application/json")
            return response

        team_one = request.POST.get('osn-1')
        team_two = request.POST.get('osn-2')

        if team_one:
            betevent = BetEvent.objects.create(user=request.user, line=linebets,
                                     ration=linebets.team_one_ration, amount=amount, team=1, main=True)
            bet = Bet.objects.create(user=request.user, line=linebets,
                                     event=betevent,)
            data['success'] = 'Ставка успешно сделана'
        elif team_two:
            betevent = BetEvent.objects.create(user=request.user, line=linebets,
                                     ration=linebets.team_two_ration, amount=amount, team=2, main=True)
            bet = Bet.objects.create(user=request.user, line=linebets,
                                     event=betevent,)
            data['success'] = 'Ставка успешно сделана'


        #data['html'] = render_to_string("core/table/ajax.html", context)
        response = HttpResponse(json.dumps({'data':data}), content_type="application/json")
        return response

#ставка если есть доп события
def linetable_many(request):
    context = RequestContext(request)
    data = {}
    print(request.POST)

    #Проверка активно ли событие
    linebets = LineBets.objects.get(id=request.POST.get('line'))
    if linebets.is_finish:
        data['error'] = 'Событие завершено'
        response = HttpResponse(json.dumps({'data':data}), content_type="application/json")
        return response
    elif linebets.is_cancel:
        data['error'] = 'Событие отменено'
        response = HttpResponse(json.dumps({'data':data}), content_type="application/json")
        return response

    team_one = request.POST.get('osn-1')
    team_two = request.POST.get('osn-2')
    amount = 0
    col = 0
    if team_one != '':
        amount = amount + int(team_one)
        col += 1
    if team_two != '':
        amount = amount + int(team_two)
        col += 1
    else:
        pass

    for dop in linebets.dop_event.all():
            one = request.POST.get('dop-'+str(dop.id)+'-1')
            two = request.POST.get('dop-'+str(dop.id)+'-2')
            if one:
                amount = amount + int(one)
                col += 1
            elif two:
                amount = amount + int(two)
                col += 1
            else:
                pass

    #Проверка суммы
    if int(amount) > int(request.user.amount):
            data['error'] = 'Недостаточно средств'.decode("utf8")
            response = HttpResponse(json.dumps({'data':data}), content_type="application/json")
            return response
    elif int(amount) < int(settings.MIN_SUMMA):
            data['error'] = 'Сумма для ставки должна быть больше 50 руб.'.decode("utf8")
            response = HttpResponse(json.dumps({'data':data}), content_type="application/json")
            return response
    elif int(amount) > int(settings.MAX_SUMMA):
            data['error'] = 'Сумма для ставки превышает 1000 руб.'.decode("utf8")
            response = HttpResponse(json.dumps({'data':data}), content_type="application/json")
            return response


    if int(col) == 0:
        data['error'] = 'Ни одно событие не выбрано'.decode("utf8")
        response = HttpResponse(json.dumps({'data':data}), content_type="application/json")
        return response
    elif int(col) > 1:
        betexpress  = BetExpress.objects.create(user=request.user, line=linebets)
        if team_one:
            betevent = BetEvent.objects.create(user=request.user, line=linebets,
                                     ration=linebets.team_one_ration, amount=team_one, team=1, main=True)
            betexpress.expressone.add(betevent)
            betexpress.save()

        elif team_two:
            betevent = BetEvent.objects.create(user=request.user, line=linebets,
                                     ration=linebets.team_two_ration, amount=team_two, team=2, main=True)
            betexpress.expressone.add(betevent)
            betexpress.save()
        else:
            pass
        for dop in linebets.dop_event.all():
            one = request.POST.get('dop-'+str(dop.id)+'-1')
            two = request.POST.get('dop-'+str(dop.id)+'-2')
            if one:
                event = Event.objects.get(id=dop.id)
                betevent = BetEvent.objects.create(user=request.user, line=linebets, event=event,
                                     ration=event.team_one_ration, amount=one, team=1)
                betexpress.expressone.add(betevent)
                betexpress.save()
                amount = amount + int(one)
                col += 1
            elif two:
                event = Event.objects.get(id=dop.id)
                betevent = BetEvent.objects.create(user=request.user, line=linebets, event=event,
                                     ration=event.team_two_ration, amount=two, team=2)
                betexpress.expressone.add(betevent)
                betexpress.save()
                amount = amount + int(two)
                col += 1
            else:
                pass
    elif int(col) == 1:
        if team_one:
            betevent = BetEvent.objects.create(user=request.user, line=linebets,
                                     ration=linebets.team_one_ration, amount=team_one, team=1, main=True)
            bet = Bet.objects.create(user=request.user, line=linebets,
                                     event=betevent,)


        elif team_two:
            betevent = BetEvent.objects.create(user=request.user, line=linebets,
                                     ration=linebets.team_two_ration, amount=team_two, team=2, main=True)
            bet = Bet.objects.create(user=request.user, line=linebets,
                                     event=betevent,)

        else:
            pass
        for dop in linebets.dop_event.all():
            one = request.POST.get('dop-'+str(dop.id)+'-1')
            two = request.POST.get('dop-'+str(dop.id)+'-2')
            if one:
                event = Event.objects.get(id=dop.id)
                betevent = BetEvent.objects.create(user=request.user, line=linebets, event=event,
                                     ration=event.team_one_ration, amount=one, team=1)
                bet = Bet.objects.create(user=request.user, line=linebets,
                                     event=betevent,)
                amount = amount + int(one)
                col += 1
            elif two:
                event = Event.objects.get(id=dop.id)
                betevent = BetEvent.objects.create(user=request.user, line=linebets, event=event,
                                     ration=event.team_two_ration, amount=two, team=2)
                bet = Bet.objects.create(user=request.user, line=linebets,
                                     event=betevent,)
                amount = amount + int(two)
                col += 1
            else:
                pass





    print(col)

    data['success'] = 'Ставка успешно сделана'

    response = HttpResponse(json.dumps({'data':data}), content_type="application/json")
    return response

class ExpressView(ListView):
    queryset = LineBets.objects.filter(is_active=True).order_by('date_start')
    template_name = 'core/linetable_express.html'

    def get_queryset(self):
        qs = super(ExpressView, self).get_queryset()
        return qs

    def render_to_response(self, context, **response_kwargs):
        response = super(ExpressView, self).render_to_response(context, **response_kwargs)
        return response

    def get_context_data(self, **kwargs):
        context = super(ExpressView, self).get_context_data(**kwargs)
        return context

    def post(self, request, *args, **kwargs ):
        self.object_list = self.get_queryset()
        context = self.get_context_data()

        if request.POST.get('amount') == '' or int(request.POST.get('amount')) >= int(request.user.amount):
            context['error'] = 'Недостаточно денежных средств'
            return self.render_to_response(context)

        linet = []
        for exp in self.get_queryset():
            if 'exp_'+str(exp.id)+str(1) in request.POST:
                lines = BetExpressOne()
                lines.user = request.user
                lines.line = exp
                lines.ration = exp.team_one_ration
                lines.team = 1
                lines.save()
                linet.append(lines.id)
            elif 'exp_'+str(exp.id)+str(2) in request.POST:
                lines = BetExpressOne()
                lines.user = request.user
                lines.line = exp
                lines.ration = exp.team_two_ration
                lines.team = 2
                lines.save()
                linet.append(lines.id)
            else:
                print('fail')
        if linet:
            items = BetExpressOne.objects.filter(id__in=linet)
            betexp = BetExpress()
            betexp.user = request.user
            betexp.amount = request.POST.get('amount')
            betexp.save()
            for item in items:
                betexp.expressone.add(item)
            betexp.save()

        return self.render_to_response(context)

def sert(request):
    BASE_DIR = os.path.dirname('1EDEF48E2BFBCD73E1C4E6C164816E3D.txt')
    print(BASE_DIR)
    path = '/home/eagaming/1EDEF48E2BFBCD73E1C4E6C164816E3D.txt'
    fsock = open(path,"rb")

    response = HttpResponse(fsock, content_type = mimetypes.guess_type(path)[0])
    response['Content-Length'] = os.path.getsize(path) # not FileField instance
    response['Content-Disposition'] = 'attachment; filename=%s' % os.path.basename(path)# same here
    return response


class TestView(TemplateView):
    template_name = 'core/test.html'

    def get_context_data(self, **kwargs):
        context = super(TestView, self).get_context_data(**kwargs)
        return context
