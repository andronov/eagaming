# -*- coding: utf-8 -*-
from django.db import models
from django.db.models.signals import post_save
from easy_thumbnails.fields import ThumbnailerImageField
from django.utils.translation import ugettext, ugettext_lazy as _
from django.core.urlresolvers import reverse
from accounts import UserProfile


class Country(models.Model):
    name = models.CharField(_('Name'), max_length=40, blank=True)
    nameshort = models.CharField(_('Nameshort'), max_length=3, blank=True)
    img = ThumbnailerImageField(upload_to='country', blank=True)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
            return self.name



class Discipline(models.Model):
    name = models.CharField(_('Name'), max_length=60, blank=True)
    img = ThumbnailerImageField(upload_to='desc/', blank=True)
    small = ThumbnailerImageField(_(u'Иконка'), upload_to='desc/', blank=True)
    created = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(_('active'), default=True)

    def __unicode__(self):
            return self.name





class Turnir(models.Model):
    """
    Класс Соревнования
    """
    name = models.CharField(_('Name'), max_length=40, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(_('active'), default=True)

    def __unicode__(self):
            return self.name




class Team(models.Model):
    """
    Класс Команды
    """
    name = models.CharField(_('Name'), max_length=120, blank=True)
    country = models.ForeignKey(Country, related_name='team_country', blank=True)
    img = ThumbnailerImageField(upload_to='team/', blank=True)
    is_active = models.BooleanField(_('active'), default=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
            return self.name


class Event(models.Model):
    """
    Класс события, ставки
    """
    name = models.CharField(_('Name'), max_length=120, blank=True)


    team_one_ration = models.FloatField(null=True, verbose_name="Коэффициент победы первой команды")
    team_two_ration = models.FloatField(null=True, verbose_name="Коэффициент победы второй команды")

    team_one_win = models.BooleanField(_(u'Первая команда победила?'), default=False, blank=True)
    team_two_win = models.BooleanField(_(u'Вторая команда победила?'), default=False, blank=True)

    is_active = models.BooleanField(_(u'Событие активно'), default=True)
    is_finish = models.BooleanField(_(u'Событие закончилось'), default=False)
    is_cancel = models.BooleanField(_(u'Событие отменено'), default=False)

    date_start = models.DateTimeField(blank=True, verbose_name=u"Дата начала события")
    date_finish = models.DateTimeField(blank=True, verbose_name=u"Дата окончания события")
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __unicode__(self):
            return self.name

class LineBets(models.Model):
    """
    Класс линии
    """
    discipline = models.ForeignKey(Discipline, related_name='line_discipline', verbose_name="Дисциплина")
    turnir = models.ForeignKey(Turnir, related_name='line_turnir', blank=True, verbose_name="Турнир")
    team_one = models.ForeignKey(Team, related_name='line_one_team', verbose_name="Первая команда")
    team_two = models.ForeignKey(Team, related_name='line_two_team', verbose_name="Вторая команда")
    dop_event = models.ManyToManyField(Event, related_name='line_dop_event',blank=True, null=True,verbose_name="Дополнительные события")

    team_one_ration = models.FloatField(null=True, verbose_name="Коэффициент победы первой команды")
    team_two_ration = models.FloatField(null=True, verbose_name="Коэффициент победы второй команды")

    team_one_win = models.BooleanField(_(u'Первая команда победила?'), default=False, blank=True)
    team_two_win = models.BooleanField(_(u'Вторая команда победила?'), default=False, blank=True)

    is_active = models.BooleanField(_(u'Событие активно'), default=True)
    is_finish = models.BooleanField(_(u'Событие закончилось'), default=False)
    is_cancel = models.BooleanField(_(u'Событие отменено'), default=False)

    slug = models.SlugField(_("slug"))

    video = models.CharField(_('video'), max_length=250, blank=True, null=True)

    date_start = models.DateTimeField(blank=True, verbose_name=u"Дата начала события")
    date_finish = models.DateTimeField(blank=True, verbose_name=u"Дата окончания события")
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def get_absolute_url(self):
        return reverse('detailline', args=[self.slug])

    def __unicode__(self):
            return self.discipline.name

    class Meta:
        verbose_name = "Линия ставок"
        verbose_name_plural = "Линии ставок"

class BetEvent(models.Model):
    user = models.ForeignKey(UserProfile, related_name='betevent_user')
    line = models.ForeignKey(LineBets, related_name='betevent_line')
    event = models.ForeignKey(Event, related_name='betevent_event',  blank=True, null=True)
    ration = models.FloatField(null=True, verbose_name="Коэффициент")
    amount = models.FloatField(_(u'Cумма ставки общая'), blank=True, null=True)

    team = models.PositiveIntegerField(default=0)

    position_team = models.PositiveIntegerField(_(u'Результат'), blank=True, null=True)
    finish = models.BooleanField(_(u'Завершено'), default=False)
    main = models.BooleanField(_(u'Ставка на главное событие'), default=False)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)



class Bet(models.Model):
    user = models.ForeignKey(UserProfile, related_name='bet_user')
    line = models.ForeignKey(LineBets, related_name='bet_line')
    event = models.ForeignKey(BetEvent, related_name='bet_event',  blank=True, null=True)

    finish = models.BooleanField(_(u'Завершено'), default=False)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def all_amount(self):
        return self.event.amount

    def all_ration(self):
        return self.event.ration

    #результат
    def result(self):
        result = False
        if self.finish and self.event.finish:
            if int(self.event.team == self.event.position_team):
                result = 'Yes'
            else:
                result = 'No'
        else:
            result = 'On'
        return result


    #результат выиграша
    def one_count(self):
        return self.all_ration()*self.all_amount()


class BetExpress(models.Model):
    user = models.ForeignKey(UserProfile, related_name='express_user')
    line = models.ForeignKey(LineBets, related_name='betex_line')
    expressone = models.ManyToManyField(BetEvent, related_name='expressone',  blank=True, null=True)

    amount = models.FloatField(_(u'Cумма ставки общая'), blank=True, null=True)
    finish = models.BooleanField(_(u'Завершено'), default=False)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def all_ration(self):
        ration = 1
        if self.expressone.all():
            for s in self.expressone.all():
                ration = ration * s.ration
        return round(ration, 3)

    def all_amount(self):
        amount = 0
        if self.expressone.all():
            for s in self.expressone.all():
                amount = int(amount) + int(s.amount)
        return amount

#списсываем сумму после добавления ставки
def out_user_summa(sender, instance, **kwargs):
    if kwargs["created"]:
        users = UserProfile.objects.get(id=instance.user.id)
        itog = int(users.amount) - int(instance.amount)
        users.amount = int(itog)
        users.save()


# register the signal
post_save.connect(out_user_summa, sender=BetEvent, dispatch_uid="out_user_summa")