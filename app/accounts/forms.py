from registration.forms import RegistrationFormUniqueEmail
from django import forms
from django.contrib.auth import authenticate
from django.forms import ModelForm
from registration.models import RegistrationProfile
from accounts.models import UserProfile as User
from django.utils.translation import ugettext, ugettext_lazy as _


class CustomRegistrationForm(forms.Form):
    """
    Form for registering a new user account.

    Validates that the requested username is not already in use, and
    requires the password to be entered twice to catch typos.

    Subclasses should feel free to add any additional validation they
    need, but should avoid defining a ``save()`` method -- the actual
    saving of collected user data is delegated to the active
    registration backend.

    """
    required_css_class = 'required'

    username = forms.RegexField(regex=r'^[\w.@+-]+$',
                                max_length=30,
                                label=_("Username"),
                                error_messages={'invalid': _("This value may contain only letters, numbers and @/./+/-/_ characters.")})
    email = forms.EmailField(label=_("E-mail"))
    password1 = forms.CharField(widget=forms.PasswordInput,
                                label=_("Password"))

    def clean_username(self):
        """
        Validate that the username is alphanumeric and is not already
        in use.

        """
        existing = User.objects.filter(username__iexact=self.cleaned_data['username'])
        if existing.exists():
            raise forms.ValidationError(_("A user with that username already exists."))
        else:
            return self.cleaned_data['username']

    def clean(self):
        """
        Verifiy that the values entered into the two password fields
        match. Note that an error here will end up in
        ``non_field_errors()`` because it doesn't apply to a single
        field.

        """
        if 'password1' in self.cleaned_data and 'password2' in self.cleaned_data:
            if self.cleaned_data['password1'] != self.cleaned_data['password2']:
                raise forms.ValidationError(_("The two password fields didn't match."))
        return self.cleaned_data

class CustomRegistrationFormUniqueEmail(CustomRegistrationForm):
    """
    Subclass of ``RegistrationForm`` which enforces uniqueness of
    email addresses.

    """
    def clean_email(self):
        """
        Validate that the supplied email address is unique for the
        site.

        """
        if User.objects.filter(email__iexact=self.cleaned_data['email']):
            raise forms.ValidationError(_("This email address is already in use. Please supply a different email address."))
        return self.cleaned_data['email']

class CustomUserRegistrationForm(CustomRegistrationFormUniqueEmail):
    #referral_code = forms.CharField(max_length=6, required=False)
    def save(self, profile_callback=None):
        new_user = RegistrationProfile.objects.create_inactive_user(username=self.cleaned_data['username'],
                        password=self.cleaned_data['password1'],
                        email=self.cleaned_data['email'],
                        site='')

        #cuser = CustomUserRegistration(user=new_user)
        #cuser = CustomUserRegistration(user=new_user,
        #                               referral_code = self.cleaned_data['referral_code'])
        return new_user


class SettingsForm(forms.ModelForm):
     email = forms.CharField(label='Email')
     username = forms.RegexField(regex=r'^[\w.@+-]+$',
                                max_length=30,
                                label=_("Username"),
                                error_messages={'invalid': _("This value may contain only letters, numbers and @/./+/-/_ characters.")})
     avatar = forms.FileField(required=False)


     class Meta:
        model = User
        fields = ()

class PaymentSettingsForm(forms.ModelForm):
     webmoney = forms.IntegerField(label='WebMoney')
     qiwi = forms.IntegerField(label='Qiwi')

     class Meta:
        model = User
        fields = ()

class SecuritySettingsForm(forms.ModelForm):
    """
    A form that creates a user, with no privileges, from the given username and
    password.
    """
    error_messages = {
        'password_mismatch': _("The two password fields didn't match."),
    }
    password1 = forms.CharField(label=_("Password"),
        widget=forms.PasswordInput)
    passwords = forms.CharField(label=_("Passwords"),
        widget=forms.PasswordInput)
    password2 = forms.CharField(label=_("Password confirmation"),
        widget=forms.PasswordInput,
        help_text=_("Enter the same password as above, for verification."))

    class Meta:
        model = User
        fields = ()

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")

        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )

        return password2

    def save(self, commit=True):
        user = super(SecuritySettingsForm, self).save(commit=False)
        old_pass = self.cleaned_data['passwords']
        users = authenticate(username=str(user.username), password=str(old_pass))
        print(user,users)
        if users is  None:
            print('dfdfgdf')
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )
        else:

            user.set_password(self.cleaned_data["password1"])
            #if commit:
            user.save()
        return user

