import hashlib
import json
from django.contrib.auth import authenticate
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404, render_to_response, redirect, resolve_url
from django.template import RequestContext
from django.views.generic import TemplateView, FormView, UpdateView, ListView, DetailView
from endless_pagination.views import AjaxListView
import time
from accounts.forms import SettingsForm, PaymentSettingsForm, SecuritySettingsForm, CustomUserRegistrationForm
from accounts.models import UserProfile, Referal, ReferalHistory
from core.models import Bet, BetExpress

"""
class SettingsView(TemplateView):

    template_name = 'accounts/settings.html'

    def get_context_data(self, **kwargs):
        context = super(SettingsView, self).get_context_data(**kwargs)

        return context
"""
class SettingsView(UpdateView):
    template_name = 'accounts/settings.html'
    model = UserProfile
    form_class = SettingsForm
    #fields = ['user.email']
    success_url = '/accounts/settings'

    def form_valid(self, form):
        self.object = form.save(commit=False)

        self.object.email = form.cleaned_data['email']

        self.object.username = form.cleaned_data['username']
        self.object.name = form.cleaned_data['username']
        try:
            z = self.get_form_kwargs().get('files')['avatar']
        except:
            z = ''
        if z == '':
            self.object.avatar = ''
        else:
            self.object.avatar = self.get_form_kwargs().get('files')['avatar']
        self.object.save()

        #return redirect(resolve_url('settings'))

        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

    def get_object(self, queryset=None):
            return self.request.user

    def get_context_data(self, **kwargs):
        context = super(SettingsView, self).get_context_data(**kwargs)
        context['avatar'] = self.get_object().avatar
        return context

    def get_initial(self):
        initial={'username': str(self.get_object().username), 'user':self.get_object(), 'email': str(self.get_object().email)}
        return initial

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
                return redirect('auth_login')
        return super(SettingsView, self).dispatch(*args, **kwargs)

"""
class SettingsView(DetailView):
    template_name = 'accounts/settings.html'
    model = UserProfile
    success_url = '/accounts/settings'

    def get_context_data(self, **kwargs):
        context = super(SettingsView, self).get_context_data(**kwargs)
        context['avatar'] = self.get_object().avatar
        return context

    def post(self, request, *args, **kwargs ):
        context = RequestContext(request)
        data = {}

        print(str(request.POST['name']).re.compile('^[\w.@+-]+$'))

        response = HttpResponse(json.dumps({'data':data}), content_type="application/json")
        return response

    def get_object(self, queryset=None):
            return self.request.user

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
                return redirect('auth_login')
        return super(SettingsView, self).dispatch(*args, **kwargs)
"""

class PaymentSettingsView(UpdateView):
    template_name = 'accounts/settings.html'
    model = UserProfile
    form_class = PaymentSettingsForm
    #fields = ['user.email']
    success_url = '/accounts/settings/payment'

    def form_valid(self, form):
        self.object = form.save(commit=False)

        self.object.webmoney = form.cleaned_data['webmoney']
        self.object.qiwi = form.cleaned_data['qiwi']
        self.object.save()

        return self.render_to_response(self.get_context_data(form=form))
        #return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

    def get_object(self, queryset=None):
            return self.request.user

    def get_context_data(self, **kwargs):
        context = super(PaymentSettingsView, self).get_context_data(**kwargs)
        return context

    def get_initial(self):
        initial={'webmoney': str(self.get_object().webmoney), 'qiwi':str(self.get_object().qiwi)}
        return initial

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
                return redirect('auth_login')
        return super(PaymentSettingsView, self).dispatch(*args, **kwargs)

class SecuritySettingsView(UpdateView):
    template_name = 'accounts/settings.html'
    model = UserProfile
    form_class = SecuritySettingsForm
    #fields = ['user.email']
    success_url = '/accounts/settings/security'

    def form_valid(self, form):
        #self.object = form.save(commit=False)


        return self.render_to_response(self.get_context_data(form=form))


    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

    def get_object(self, queryset=None):
            return self.request.user

    def get_context_data(self, **kwargs):
        context = super(SecuritySettingsView, self).get_context_data(**kwargs)
        return context

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
                return redirect('auth_login')
        return super(SecuritySettingsView, self).dispatch(*args, **kwargs)


def _createHash():
    """
    Function for creating 6 symbols random hash for referral code
    """
    hash = hashlib.md5()
    hash.update(str(time.time()))
    return hash.hexdigest()[:10]

def registration(request, code=''):
    """
    Registration view with additional field referral code and add user to group
    """
    form = CustomUserRegistrationForm()
    if request.method == 'POST':
        form = CustomUserRegistrationForm(request.POST)
        print(form.errors)
        if form.is_valid():
            form.save()
            print(form.cleaned_data['username'])
            user = UserProfile.objects.get(username=form.cleaned_data['username'])
            user.name = _createHash()
            user.save()
            # try to find referrer
            ref = Referal.objects.create(user_id=int(user.id))
            print(ref)
            if 'ref' in request.COOKIES:
                     refffer = Referal.objects.get(user_id=int(request.COOKIES['ref']))
                     refer = ReferalHistory.objects.create(ref=refffer, refer=user)
                     print(refer)

            # check and update VisitirTrack
            # VisitorTrack.objects.track_registration(request)

            # send email notifications
            #from notification import models as notification
            #notification.send([user], "registration_complete", {})
            variables = RequestContext(request, {'yes': True})
            return render_to_response('registration/registration_form.html', variables)

    variables = RequestContext(request, {'form': form})
    return render_to_response('registration/registration_form.html', variables)


class RatesView(AjaxListView):
    queryset = Bet.objects.order_by('-created')#filter(event__isnull=True)
    template_name = 'core/rates_list.html'

    def get_queryset(self):
        qs = super(RatesView, self).get_queryset()
        qs = qs.filter(user=self.request.user)
        return qs

    def render_to_response(self, context, **response_kwargs):
        response = super(RatesView, self).render_to_response(context, **response_kwargs)
        return response

    def get_context_data(self, **kwargs):
        context = super(RatesView, self).get_context_data(**kwargs)
        return context

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
                return redirect('auth_login')
        return super(RatesView, self).dispatch(*args, **kwargs)

class HistoryExpressView(ListView):
    queryset = BetExpress.objects.order_by('-created')
    template_name = 'core/express_list.html'

    def get_queryset(self):
        qs = super(HistoryExpressView, self).get_queryset()
        qs = qs.filter(user=self.request.user)
        return qs

    def render_to_response(self, context, **response_kwargs):
        response = super(HistoryExpressView, self).render_to_response(context, **response_kwargs)
        return response

    def get_context_data(self, **kwargs):
        context = super(HistoryExpressView, self).get_context_data(**kwargs)
        return context

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
                return redirect('auth_login')
        return super(HistoryExpressView, self).dispatch(*args, **kwargs)

class ReferalView(ListView):
    queryset = Referal.objects.filter().order_by('created')
    template_name = 'core/referal_list.html'

    def get_queryset(self):
        qs = super(ReferalView, self).get_queryset()
        qs.filter(user_id=self.request.user.id)
        return qs

    def render_to_response(self, context, **response_kwargs):
        response = super(ReferalView, self).render_to_response(context, **response_kwargs)
        return response

    def get_context_data(self, **kwargs):
        context = super(ReferalView, self).get_context_data(**kwargs)
        print(context)
        return context

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
                return redirect('auth_login')
        return super(ReferalView, self).dispatch(*args, **kwargs)