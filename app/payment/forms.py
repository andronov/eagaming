# -*- coding: utf-8 -*-
from django.forms import ModelForm
from payment.models import BidPayment


class PaymentForm(ModelForm):
    class Meta:
        model = BidPayment

    fields = '__all__'