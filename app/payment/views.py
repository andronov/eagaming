# -*- coding: utf-8 -*-
import json
import datetime
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
import hashlib
# Create your views here.
from django.template import RequestContext
from django.template.loader import render_to_string
from django.views.generic import TemplateView, ListView, DetailView
from django.contrib import messages
from payment.forms import PaymentForm
from payment.models import *


class PaymentView(TemplateView):
    template_name = 'payment/pay.html'

    def get_context_data(self, **kwargs):
        context = super(PaymentView, self).get_context_data(**kwargs)
        context['payments'] = PaymentSum.objects.all()
        return context

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
                return redirect('auth_login')
        return super(PaymentView, self).dispatch(*args, **kwargs)

class CashoutView(TemplateView):
    template_name = 'payment/cashout.html'

    def get_context_data(self, **kwargs):
        context = super(CashoutView, self).get_context_data(**kwargs)
        context['form'] = PaymentForm()
        context['bids'] = BidPayment.objects.all().order_by('-created')
        return context

    def post(self, request, *args, **kwargs ):
        context = RequestContext(request)
        data = {}
        print(request.POST)
        if request.POST['summa'] == '':
            data['error'] = "Введите сумму!"
            response = HttpResponse(json.dumps({'data':data}), content_type="application/json")
            return response
        elif request.POST['number'] == '':
            data['error'] = "Введите номер телефона!"
            response = HttpResponse(json.dumps({'data':data}), content_type="application/json")
            return response
        if int(request.POST['summa']) > int(request.user.amount):
            data['error'] = "Сумма на вывод первышает допустимую!"
            response = HttpResponse(json.dumps({'data':data}), content_type="application/json")
            return response
        else:
            bid = BidPayment.objects.create(user=request.user, system=int(request.POST['sistema']),
                                            sum=int(request.POST['summa']), number=int(request.POST['number']))
            data['success'] = "Заявка успешно создана!"

        response = HttpResponse(json.dumps({'data':data}), content_type="application/json")
        return response

    def dispatch(self, *args, **kwargs):
        if not self.request.user.is_authenticated():
                return redirect('auth_login')
        return super(CashoutView, self).dispatch(*args, **kwargs)

def payment(request):
    merchant_id = settings.ID_MAGAZINE
    secret_key = settings.ID_SECRET_KEY
    if 'sum' in request.GET:
        out_amount = request.GET['sum']
        order = Order.objects.create(user=request.user, sum=out_amount)
        str_h = merchant_id + ':' + str(out_amount) + ':' + secret_key + ':' + str(order.id)
        hsh = hashlib.md5()
        hsh.update(str_h)
        signature = hsh.hexdigest()
        url = 'http://www.free-kassa.ru/merchant/cash.php?m=' + merchant_id + '&oa=' + \
              str(out_amount) + '&s=' + str(signature) + '&o=' + str(order.id)
        return HttpResponseRedirect(url)
    elif 'summa' in request.GET:
        out_amount = request.GET['summa']
        order = Order.objects.create(user=request.user, sum=out_amount)
        str_h = merchant_id + ':' + str(out_amount) + ':' + secret_key + ':' + str(order.id)
        hsh = hashlib.md5()
        hsh.update(str_h)
        signature = hsh.hexdigest()
        url = 'http://www.free-kassa.ru/merchant/cash.php?m=' + merchant_id + '&oa=' + \
              str(out_amount) + '&s=' + str(signature) + '&o=' + str(order.id)
        return HttpResponseRedirect(url)