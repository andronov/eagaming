# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from datetime import datetime
from accounts import UserProfile


class PaymentSum(models.Model):
    sum = models.PositiveIntegerField(default=1)
    gift = models.PositiveIntegerField(default=1)

class BidPayment(models.Model):
    TYPE_WHO = 0
    TYPE_REP = 1
    TYPE_LIST = 2

    TYPE_CHOICES = (
        (TYPE_WHO, 'Ожидает'),
        (TYPE_REP, 'В процессе'),
        (TYPE_LIST, 'Выплачено'),
    )
    TYPE_ONE = 0
    TYPE_TWO = 1

    TYPE_SYSTEM = (
        (TYPE_ONE, 'WebMoney'),
        (TYPE_TWO, 'QIWI'),
    )
    user = models.ForeignKey(UserProfile, related_name='user_bid')
    result = models.IntegerField(choices=TYPE_CHOICES, blank=True, null=True, default=0)
    system = models.IntegerField(choices=TYPE_SYSTEM, blank=True, null=True, default=0)
    sum = models.PositiveIntegerField(default=0)
    number = models.CharField(_('Номер кошелька'), max_length=15, blank=True, null=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

class Order(models.Model):
    user = models.ForeignKey(UserProfile,related_name='order-user')
    sum = models.PositiveIntegerField(default=0, blank=True, null=True)
    good = models.BooleanField(default=False)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)