# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _
from datetime import datetime



class List(models.Model):
    title = models.CharField(_("Title"), max_length=250)
    slug = models.SlugField(_("Slug"), max_length=250, help_text='you can leave this field blank')
    short_text = models.TextField(_("Short_Text"), blank=True, null=True)
    text = models.TextField(_("Text"), blank=True, null=True)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _('List')
        verbose_name_plural = _('Lists')


class PublishManager(models.Manager):
    def get_query_set(self):
        return Article.objects.filter(publish=True)


class Article(models.Model):
    title = models.CharField(_("Title"), max_length=250)
    slug = models.SlugField(_("Slug"), max_length=250, help_text='you can leave this field blank')
    image = models.ImageField(_("Image"), upload_to='Article', blank=True, null=True)
    short_text = models.TextField(_("Short_Text"), blank=True, null=True)
    text = models.TextField(_("Text"), blank=True, null=True)
    datetime = models.DateTimeField(_("Datetime"), blank=True, null=True)
    article_list = models.ForeignKey("List", related_name="articles")
    category = models.ForeignKey("Category", related_name="article_categories", blank=True, null=True)
    seo_title = models.CharField(_("Seo_Title"), max_length=250, blank=True, null=True)
    seo_description = models.CharField(_("seo_desctiption"), max_length=250, blank=True, null=True)
    seo_keywords = models.CharField(_("seo_keywords"), max_length=250, blank=True, null=True)
    index = models.BooleanField(_("Index"), default=False, blank=True)
    publish = models.BooleanField(_('Publish'), default=False)
    visitors = models.IntegerField(_('visitors'), default=0)

    objects = models.Manager()
    get_data = PublishManager()

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _('Article')
        verbose_name_plural = _('Articles')
        ordering = ['datetime', ]

    def get_absolute_url(self):
        if self.category:
           slug =  "/" + self.slug + "/"

        #if self.article_list.slug == 'blog':
            #slug = "/" + self.slug + "/"
        return slug

    # def comments_count(self):
    #     return self.comments.count()

    def check_parent(self, slug):

        if not self.category:
            if self.article_list.slug == slug:
                return True
        if self.category:
            if self.category.slug == slug:
                return True
            else:
                return False
        return False

    def random_articles(self):
        return Article.objects.all().exclude(id=self.id).order_by('?')


class Category(models.Model):
    title = models.CharField(_("Title"), max_length=250)
    slug = models.SlugField(_("Slug"), max_length=250, help_text='you can leave this field blank')
    image = models.ImageField(_("Image"), upload_to='Category', blank=True, null=True)
    short_text = models.TextField(_("Short_Text"), blank=True, null=True)
    text = models.TextField(_("Text"), blank=True, null=True)
    datetime = models.DateTimeField(_("Datetime"), blank=True, null=True)
    seo_title = models.CharField(_("Seo_Title"), max_length=250, blank=True, null=True)
    category_list = models.ForeignKey("List", related_name="list_categories", blank=True, null=True)
    publish = models.BooleanField(_('Publish'), default=False)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')

    def get_absolute_url(self):
        return "/" + self.category_list.slug + "/" + self.slug + "/"

    def check_parent(self, slug):
        if self.category_list.slug == slug:
            return True
        else:
            return False