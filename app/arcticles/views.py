# -*- coding: utf-8 -*-
from django.utils.translation import get_language
from models import List, Article, Category
from django.views.generic import ListView, DetailView


class ArticleDetail(DetailView):
    """
    Return Article by slug
    """
    queryset = Article.objects.filter(publish=True)
    template_name = 'arcticles/faq_detail.html'

    def get_queryset(self):
        qs = super(ArticleDetail, self).get_queryset()
        #qs = qs.exclude(text_en=u'').exclude(text_en__isnull=True)
        return qs

    def get_context_data(self, **kwargs):
        context = super(ArticleDetail, self).get_context_data(**kwargs)
        context['articles'] = Article.objects.filter(article_list__slug='faq', publish=True)
        print(context)
        return context

"""
class HelpListView(ListView):

    queryset = Article.objects.filter(article_list__slug='faq', publish=True)
    template_name = 'arcticles/faq_list.html'


    def get_queryset(self):
        qs = super(HelpListView, self).get_queryset()

        #qs = qs.exclude(text_en=u'').exclude(text_en__isnull=True)
        return qs

    def get_context_data(self, **kwargs):
        context = super(HelpListView, self).get_context_data(**kwargs)
        context['category'] = Category.objects.all()
        return context
"""
"""
class FAQListView(ListView):

    queryset = Category.objects.filter(publish=True)
    template_name = 'arcticles/faq_list.html'
    
    def get_context_data(self, **kwargs):
        context = super(FAQListView, self).get_context_data(**kwargs)


        faqs = []
        articles = Article.objects.filter(article_list__title='faq', publish=True)
        if articles:
                faqs.append({'category': '1', 'article_list': articles})
        context['category'] = Category.objects.all()
        context['faqs'] = faqs

        return context
"""