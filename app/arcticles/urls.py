from django.conf.urls import patterns, url
from arcticles.models import Article
from views import ArticleDetail
from django.views.generic import DetailView

urlpatterns = patterns('',
    #url(r'^faq/$', HelpListView.as_view(), name='faq'),
    #url(r'^blog/$', BlogListView.as_view(), name='blog'),
    # url(r'^blog/(?P<slug>[-\w]+)/$', ArticleDetail.as_view(), name='blog-article'),
    url(r'^(?P<slug>[-\w]+)/$', ArticleDetail.as_view(), name='common-article'),
    )