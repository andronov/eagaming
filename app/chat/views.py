from django.shortcuts import render
from django.shortcuts import render_to_response
from django.http import HttpResponse
from accounts import UserProfile
from models import Attach
from PIL import Image, ImageOps
import cStringIO
from StringIO import StringIO
from django.core.files.uploadedfile import InMemoryUploadedFile


def test(request):
    from django.template import RequestContext
    context = {}
    #return render_to_response('chat/test.html')
    return render_to_response('chat/test.html', context, context_instance = RequestContext(request))


def upload(request):
    api_key = request.POST.get('api_key')
    print api_key
    if api_key:
        session_api_key = request.session.get('user_api_key', None)
        print session_api_key
        if not session_api_key == api_key:
            try:
                account = UserProfile.objects.get(api_key=api_key)
                request.session['user_api_key'] = account.api_key
                request.session['user_name'] = account.username
                request.session['user_id'] = account.id
                request.session['user_avatar'] = account.avatar
            except UserProfile.DoesNotExist:
                return HttpResponse('{"status": "error", "error": "api_key error"}')
    else:
        return HttpResponse('{"status": "error", "error": "not api_key"}')


    print request.session['user_id']
    print request.session['user_name']
    try:
        file = request.FILES.get('image')
        if file and not file.multiple_chunks():
            attach = Attach()

            # Save thumb
            file.open()
            file_stream = cStringIO.StringIO(file.read())
            image = Image.open(file_stream)
            imagefit = ImageOps.fit(image, (500, 375), Image.ANTIALIAS)
            tmp_thumb = StringIO()
            imagefit.save(tmp_thumb, 'JPEG', quality=95)
            tmp_thumb.seek(0)
            thumb_file = InMemoryUploadedFile(tmp_thumb, None, '%s.jpeg' % file.name, 'image/jpeg', tmp_thumb.len, None)
            attach.sender = request.session['user_id']
            attach.thumb.save(thumb_file.name, thumb_file)

            # Save file
            attach.file.save(file.name, file)

            # Save model
            attach.save()
            return HttpResponse('{"status": "ok", "id": "%s", "image_big": "%s", "image_thumb": "%s"}' % (attach.pk, attach.file.url, attach.thumb.url))
        else:
            return HttpResponse('{"status": "error", "Error": "very big file"}')
    except Exception, msg:
        print msg
        return HttpResponse('{"status": "error", "error": "not support file type"}')
