import datetime
import json
import time
import urllib
import uuid

import brukva
import redis
import tornado.web
import tornado.websocket
import tornado.ioloop
import tornado.httpclient
from tornado.websocket import WebSocketClosedError
import json
import re

from django.conf import settings
from django.utils.importlib import import_module
from accounts import UserProfile

session_engine = import_module(settings.SESSION_ENGINE)

from models import Thread, Message

r = redis.StrictRedis()

threads = []

for thread in Thread.objects.all():
    threads.append(int(thread.id))
    name_thread = 'thread_%s_last_messages' % thread.id
    thread_message_id = 'thread_%s_last_message_id' % thread.id
    r.set(thread_message_id, 0)
    print 'Messages id: 0'
    r.delete(name_thread)
    message_id = None
    messages = Message.objects.filter(thread=thread).order_by('-message_id')[:100]
    for message in reversed(messages):
        try:
            user = UserProfile.objects.get(pk=message.sender)
            username = user.username
            avatar = str(user.avatar)
            user_id = int(user.id)
        except UserProfile.DoesNotExist:
            username = 'Deleted'
            avatar = None
            user_id = 0

        image = None
        if message.attach:
            image = {'full': message.attach.file.url, 'thumb': message.attach.thumb.url}

        r.rpush(name_thread, json.dumps({
            "message_id": message.message_id,
            "timestamp": int(time.time()),
            "sender": username,
            'user_id': user_id,
            "text": message.text,
            "avatar": avatar,
            "type": "last",
            "image": image,
        }))

        message_id = message.message_id
    if len(messages) > 0:
        print messages[0].message_id
        print 'Messages id: ', message_id
        r.set(thread_message_id, message_id)

c = brukva.Client()
c.connect()


def get_tid():
    tid = 1
    while True:
        yield tid
        tid += 1


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.set_header('Content-Type', 'text/plain')
        self.write('Chat =)')


class MessagesHandler(tornado.websocket.WebSocketHandler):
    def __init__(self, *args, **kwargs):
        super(MessagesHandler, self).__init__(*args, **kwargs)
        self.client = brukva.Client()
        self.client.connect()
        self.tid = get_tid()

    def open(self, thread_id):
        session_key = self.get_cookie(settings.SESSION_COOKIE_NAME)
        session = session_engine.SessionStore(session_key)

        try:
            self.api_key = session["user_api_key"]
            self.user_id = session["user_id"]
            self.sender_name = session['user_name']
            self.api_avatar = session['user_avatar']
        except KeyError:
            self.api_key = 'o9d4fa2$ue4y=afajp$0gp&m^kp'
            self.user_id = "6767676767"
            self.sender_name = "Anonim"
            self.api_avatar = "none"
            #self.close()
            #return

        if not int(thread_id) in threads:
            self.close()
            return

        self.channel = 'thread_%s_messages' % thread_id
        self.thread_id = thread_id
        self.last_message_id = 'thread_%s_last_message_id' % thread_id
        self.last_messages = 'thread_%s_last_messages' % thread_id
        self.client.subscribe(self.channel)
        r.ltrim(self.last_messages, -100, 100)
        c.lrange(self.last_messages, 0, 100, self.on_last_messages)
        self.client.listen(self.show_new_message)

    def on_last_messages(self, result):
        print result
        new_result = '['
        if len(result) > 0:
            for r in reversed(result):
                new_result += str(r) + ','
            new_result = new_result[:-1]
            new_result += ']'
            try:
                self.write_message(new_result)
            except WebSocketClosedError:
                pass
                print 'Error last'

    def tt(self):
        pass

    def handle_request(self, response):
        pass

    def on_message(self, message):
        if not message:
            return
        if len(message) > 10000:
            return

        message_id = r.incr(self.last_message_id)
        print message_id
        message = json.loads(message)

        if message['image']:
            match = re.match('^/([a-z/\.0-9]+)', message['image']['full'])
            if match and match.group(0) == message['image']['full']:
                pass
            else:
                return

            match = re.match('^/([a-z/\.0-9]+)', message['image']['thumb'])
            if match and match.group(0) == message['image']['thumb']:
                pass
            else:
                return



        timestamp = int(time.time())

        redis_message = {
            "message_id": message_id,
            "timestamp": timestamp,
            "sender": self.sender_name,
            "text": message['message'],
            'user_id': int(self.user_id),
            "avatar": self.api_avatar,
            "type": 'new',
            "image": message['image'],
        }
        c.publish(self.channel, json.dumps(redis_message))
        redis_message['type'] = "last"
        c.rpush(self.last_messages, json.dumps(redis_message))

        http_client = tornado.httpclient.AsyncHTTPClient()
        attach_id = None
        if 'image' in message and message['image'] and 'id' in message['image']:
            attach_id = message['image']['id']
        request = tornado.httpclient.HTTPRequest(
            settings.SEND_MESSAGE_API_URL,
            headers={'Accept': 'application/json, text/javascript, */*; q=0.01',
                     'X-Requested-With': 'XMLHttpRequest'},
            method="POST",

            body=json.dumps({
                "action": "ChatApi",
                "tid": next(self.tid),
                "method": "sendMessage",
                "data": [settings.CHAT_API_KEY, message['message'].encode("utf-8"), self.thread_id, message_id, self.user_id,  timestamp, attach_id]
            }),
            validate_cert=False,
        )

        http_client.fetch(request, self.handle_request)

    def show_new_message(self, result):
        self.write_message("[%s]" % str(result.body))

    def on_close(self):
        try:
            self.client.unsubscribe(self.channel)
        except AttributeError:
            pass
        def check():
            if self.client.connection.in_progress:
                tornado.ioloop.IOLoop.instance().add_timeout(
                    datetime.timedelta(0.00001),
                    check
                )
            else:
                self.client.disconnect()
        tornado.ioloop.IOLoop.instance().add_timeout(
            datetime.timedelta(0.00001),
            check
        )

application = tornado.web.Application([
    (r"/", MainHandler),
    (r'/(?P<thread_id>\d+)/', MessagesHandler),
])
