from django.db import models
from hashlib import sha1, md5
from django.conf import settings
from os import path
import uuid


class Account(models.Model):
    id = models.IntegerField(primary_key=True)
    #is_admin = models.IntegerField()
    #is_anonymous = models.IntegerField()
    #no_fees = models.IntegerField()
    username = models.CharField(unique=True, max_length=40)
    #pass_field = models.CharField(db_column='pass', max_length=255)
    #email = models.CharField(unique=True, max_length=255, blank=True)
    #loggedip = models.CharField(db_column='loggedIp', max_length=255, blank=True)
    #is_locked = models.IntegerField()
    #failed_logins = models.IntegerField(blank=True, null=True)
    #failed_pins = models.IntegerField(blank=True, null=True)
    #sessiontimeoutstamp = models.IntegerField(db_column='sessionTimeoutStamp', blank=True, null=True)
    #pin = models.CharField(max_length=255)
    api_key = models.CharField(max_length=255, blank=True)
    #token = models.CharField(max_length=65, blank=True)
    #donate_percent = models.FloatField(blank=True, null=True)
    #ap_threshold = models.FloatField(blank=True, null=True)
    #coin_address = models.CharField(max_length=255, blank=True)
    avatar = models.CharField(max_length=255, blank=True)
    #c_skype = models.CharField(max_length=40, blank=True)
    #c_vk = models.CharField(max_length=255, blank=True)
    #c_icq = models.IntegerField(blank=True, null=True)

    #class Meta:
        #managed = False
        #db_table = 'accounts'


class Thread(models.Model):
    name = models.CharField(max_length=128)

    def __unicode__(self):
        return '%s' % self.name


class Message(models.Model):
    text = models.TextField()
    message_id = models.PositiveIntegerField()
    sender = models.PositiveIntegerField()
    thread = models.ForeignKey(Thread)
    attach = models.ForeignKey('Attach', null=True, blank=True)
    datetime = models.DateTimeField(db_index=True)
    datetime_create = models.DateTimeField(auto_now_add=True, db_index=True)


def get_path(instance, filename, dirs='None'):
    new_name = '%s' % uuid.uuid4()
    new_name = new_name.lower()
    hash_sha1 = sha1(new_name).hexdigest()
    hash_filename = '%s.%s' % (md5(new_name).hexdigest(), filename.split('.')[-1].lower())
    new_filename = path.join(dirs, hash_sha1[:3], hash_sha1[3:6], hash_filename)
    return new_filename


def get_attach_path(instance, filename):
    return get_path(instance, filename, 'attachs')


def get_tumb_path(instance, filename):
    return get_path(instance, filename, 'tumbs')


class Attach(models.Model):
    sender = models.PositiveIntegerField()
    file = models.FileField(upload_to=get_attach_path)
    thumb = models.FileField(upload_to=get_tumb_path, blank=True, null=True)