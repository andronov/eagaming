from djangorpc import Error
from importlib import import_module
from django.conf import settings
from django.contrib.auth.models import User
from accounts import UserProfile
from models import Message, Thread, Attach
import json
import time
import datetime
from django.views.decorators.csrf import csrf_protect

def set_session_api_key(method):
    def wrapper(self, *args, **kwargs):
        if 'request' in kwargs:
                request = kwargs['request']
        else:
           return Error('Not request')
    
        if request.user.is_authenticated():
                account = request.user
                request.session['user_api_key'] = 'o9d4fa2$ue4y=afajp$0gp&m^kp'
                request.session['user_name'] = account.username
                request.session['user_id'] = account.id
                request.session['user_avatar'] = str(account.avatar)
                return method(self, *args, **kwargs)
        else:
            return Error('Error api_key')

    return wrapper


class ChatApiClass(object):

    #@set_session_api_key
    def getLastMessages(self, api_key, thread, counts, messages_id, request=None):
        try:
            counts = int(counts)
        except TypeError:
            return Error('Error count type')

        try:
            messages_id = int(messages_id)
        except TypeError:
            return Error('Error message id')

        if counts > 20:
            return Error('Error counts. Max count 20')

        try:
            thread = int(thread)
        except TypeError:
            return Error('Error thread id')

        messages = Message.objects.filter(thread=thread, message_id__lt=messages_id).order_by('-message_id')[:counts]

        list_messages = []
        for message in messages:
            try:
                sender = UserProfile.objects.get(id=message.sender)
                avatar = sender.avatar
                sender = sender.username
            except UserProfile.DoesNotExist:
                sender = 'Deleted'
                avatar = None
            image = None
            if message.attach:
                image = {'full': message.attach.file.url, 'thumb': message.attach.thumb.url}
            timestamp = '%.f' % time.mktime(message.datetime.timetuple())
            list_messages.append({'message_id': message.message_id, 'timestamp': timestamp,
                                  'sender': sender, 'avatar': avatar,
                                  'image': image})
        return json.dumps(list_messages)

    #@set_session_api_key
    def sendMessage(self, chat_api_key, message, thread_id, message_id, user_id, timestamp, attach_id, request):

    

        try:
            thread = Thread.objects.get(pk=int(thread_id))
        except (Thread.DoesNotExist, TypeError):
            return Error('Error thread')

        try:
            sender = UserProfile.objects.get(pk=user_id)
        except (UserProfile.DoesNotExist, TypeError):
            return Error('Error user id')

        try:
            dt = datetime.datetime.fromtimestamp(timestamp)
        except TypeError:
            return Error('Error datetime')

        try:
            if attach_id:
                attach_id = int(attach_id)
                print attach_id

                attach = Attach.objects.get(pk=attach_id, sender=int(sender.id))
            else:
                attach = None
        except (TypeError, Attach.DoesNotExist, Exception), msg:
            print 'Error: ', msg
            return Error('Error attach id')

        message = Message(text=message, sender=sender.id, thread=thread,
                          message_id=message_id, datetime=dt, attach=attach)
        message.save()
        return {'save': True}

    #@set_session_api_key
    def getThread(self, api_key, thread_name, request):
        try:
            thread = Thread.objects.get(name=thread_name)
            return {'thread_url': 'wss://%s/apichat/chat/%s/' % (request.META.get('HTTP_HOST'), int(thread.id)),
                   'thread_id': int(thread.id)}
            #return {'thread_url': 'ws://127.0.0.1:8001/%s/' % int(thread.id),
            #        'thread_id': int(thread.id)}
        except Thread.DoesNotExist:
            return Error('Error thread')

    #@set_session_api_key
    def getAvatar(self, api_key, user_id, request):

        try:
            user = UserProfile.objects.get(pk=int(user_id))
        except (UserProfile.DoesNotExist, TypeError):
            return Error('Error user id')

        return {'avatar': str(user.avatar), 'user_id': int(user.id)}