from django.contrib import admin
from models import Account, Thread, Message, Attach


class AccountAdmin(admin.ModelAdmin):
    list_display = ('id', 'username', 'api_key', 'avatar')

class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'message_id', 'text', 'sender', 'thread', 'datetime')

class ThreadAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
 

admin.site.register(Account, AccountAdmin)
admin.site.register(Message, MessageAdmin)
admin.site.register(Thread, ThreadAdmin)
admin.site.register(Attach)