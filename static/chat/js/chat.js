

$( document ).ready(function() {
    ChatApi.ws = '';
    ChatApi.thread_id = 1;
    $('.send-js').click(function() {
        send_message();
    });

    $('.message-js').keypress(function(e) {
        if(e.which == 13) {
            send_message();
        }
    });

    $('.update-js').click(function() {
        last_message_id = $('.message').attr('id');
        if (last_message_id == undefined){
            last_message_id = 0
        }
        ChatApi.getLastMessages('85f9af28510ce2b1b12e5dca52facc629a52c40c91f819c869adf4995c32a2e8', thread_id, 20, last_message_id)
    });

    var smile = {
        1: ['(smile)', ':)', ':-)', ':=)'],
        2: ['(sad)', ':(', ':-(', ':=('],
        3: ['(laugh)', ':D', ':=D', ':d', ':-d', ':=d'],
        4: ['(cool)', '8-)', '8=)', 'B-)', 'B=)', ':-D'],
        5: ['(surprised)', ':O', ':-O', ':=O', ':o', ':-o', ':=o'],
        6: ['(wink)', ';)', ';-)', ';=)'],
        7: ['(cry)', ';(', ';-(', ';=('],
        8: ['(sweat)', '(:|'],

        9: ['(speechless)', ':-|', ':|', ':=|'],
        10: ['(kiss)', ':*', ':-*', ':=*'],
        11: ['(tongueout)', ':P', ':-P', ':=P', ':p', ':-p', ':=p'],
        12: ['(blush)', ':$', ':-$', ':=$'],
        13: ['(wonder)', ':^)'],
        14: ['(snooze)', '|-)', 'I-)', 'I=)'],
        15: ['(dull)', '|-(', '|(', '|=('],
        16: ['(inlove)', '(love)'],

        17: ['(grin)', ']:)'],
        18: ['(crossedfingers)', '(yn)', '(fingers)', '(fingerscrossed)'],
        19: ['(yawn)'],
        20: ['(puke)', ':=&'],
        21: ['(doh)'],
        22: ['(angry)', ':@', ':-@', ':=@', 'x(', 'x-(', 'X(', 'X-(', 'x=(', 'X=('],
        23: ['(wasntme)'],
        24: ['(party)', '<O)', '<o)'],

        25: ['(worry)', ':S', ':s', ':-s', ':-S', ':=s', ':=S'],
        26: ['(mmmm)', '(mmm)', '(mm)'],
        27: ['(nerd)', '8-|', 'B-|', '8|', 'B|', '8=|', 'B=|'],
        28: [':=#', ':x', ':-x', ':X', ':-X', ':#', ':-#', ':=x', ':=X'],
        29: ['(wave)', '(bye)', '(hi)'],
        30: ['(facepalm)', '(fail)'],
        31: ['(devil)'],
        32: ['(angel)', '(A)'],

        33: ['(envy)'],
        34: ['(wait)'],
        35: ['(bear)', '(hug)'],
        36: ['(kate)', '(makeup)'],
        37: ['(giggle)', '(chuckle)'],
        38: ['(clap)'],
        39: ['(think)', ':=?', ':?', ':-?'],
        40: ['(bow)'],

        41: ['(rofl)'],
        42: ['(whew)'],
        43: ['(happy)'],
        44: ['(smirk)'],
        45: ['(nod)'],
        46: ['(shake)'],
        47: ['(time)', '(impatience)', '(waiting)', '(forever)'],
        48: ['(emo)'],

        49: ['(yes)', '(y)', '(Y)', '(ok)'],
        50: ['(no)', '(n)', '(N)'],
        51: ['(handshake)'],
        //52: ['(h5)', '(hifive)', '(highfive)'],
        53: ['(heart)', '(h)', '(H)', '(l)', '(L)'],
        //54: ['(notlistening)', '(lalala)', '(lalalala)', '(lala)'],
        55: ['(squirrel)', '(heidy)'],
        56: ['(flower)', '(F)', '(f)'],

        57: ['(london)', '(rain)'],
        58: ['(sun)'],
        59: ['(tumbleweed)'],
        60: ['(music)'],
        61: ['(bandit)'],
        62: ['(tmi)'],
        63: ['(coffee)'],
        64: ['(pizza)', '(pi)'],

        65: ['(mo)', '(cash)', '($)'],
        66: ['(muscle)', '(flex)'],
        67: ['(cake)', '(^)'],
        68: ['(beer)', '(bricklayers)'],
        69: ['(drink)', '(d)', '(D)'],
        70: ['(dance)', '\\o/', '\\:D/', '\\:d/'],
        71: ['(ninja)'],
        72: ['(star)', '(*)']


    };

    var message_text = $('.message-js');

    document.set_smile = function(smile_text) {
        message_text.val(message_text.val() + smile_text);
    };

    document.set_reply = function(reply) {
        message_text.val(reply + ', ');
    };

    for (var i in smile){
        var div_smile = $('div.chat-form > div.smile');
        div_smile.append('<img onclick="set_smile(\''+ smile[i][0] + '\');" style="cursor:pointer;margin:2px;height:20px !important;width:20px !important;" src="/apichat/static/smiles/'+ i +'.png"></img>');

    };

    function get_message_smile(message) {
        for (var i in smile){
            for (var j = 0; j < smile[i].length; j ++) {
                message =message.split(smile[i][j]).join('<img style="margin:2px;height:20px !important;width:20px !important;" src="/static/smiles/'+ i +'.png"></img>');
            }
        }
        return message;
    }

    ChatApi.avatars = {};

    function get_message_html(message_id, sender, user_id, timestamp, message, avatar, image){
        avatar = '';
        if (user_id in ChatApi.avatars) {
            if (ChatApi.avatars[user_id] != 'update') {
                avatar = ChatApi.avatars[user_id];
            }
        } else {
            ChatApi.avatars[user_id] = 'update';
            ChatApi.getAvatar(ChatApi.api_key, user_id, function(event){
                ChatApi.avatars[event.user_id] = event.avatar;
                if (event.avatar != '') {
                    $('.user-avatar-' + user_id).replaceWith('<img src="/uploads/avatar/'+ event.avatar +'" alt="Generic placeholder image" class="media-object">');
                }


            });
        }

        var date = new Date(timestamp*1000);
        html = '<li id="m'+ message_id +'" class="media message">';
        html += '<a ';

        if (ChatApi.user_name != sender) {
            html += 'style="cursor:pointer;" onclick="set_reply(\''+ sender +'\');"';
        }

        html += 'class="';
        if (ChatApi.user_name == sender) {
            html += 'pull-right';
        } else {
            html += 'pull-left';
        }
        html += '" >';

        if (avatar == '') {
            html += '<i class="fa fa-user chat-default-ava user-avatar-' + user_id + '"></i>';
        } else {
            html += '<img src="/uploads/avatar/'+ avatar +'" alt="Generic placeholder image" class="media-object">';
        }
        html += '</a>';


		html += '<div class="media-body chat-pop';
        if (ChatApi.user_name == sender) {
            html += ' pull-right mod';
        }
        html += '">';


        html += '<h4 class="media-heading" ';

        if (ChatApi.user_name != sender) {
            html += 'style="cursor:pointer;" onclick="set_reply(\''+ sender +'\');"';
        }

        html += '>'+ sender +'<span class="';

        if (ChatApi.user_name == sender) {
            html += 'pull-left';
        } else {
            html += 'pull-right';
        }

        html += '"><i class="fa fa-clock-o"></i>';
        html += '<abbr title=' + timestamp + ' class="timeago">' + date.getHours()+':'+ date.getMinutes()+':'+ date.getSeconds() + '</abbr>';
        html += '</span></h4>';
        message = message.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/\n/g, '<br />');
        message = get_message_smile(message);
        html += '<p>'+ message +'</p>';
        if (image) {
            html += '<a target="_blank" href="'+ image.full +'"><img style="border-radius:0;width:auto !important;height:auto !important;" src="' + image.thumb + '" /></a>';
        }
        html += '</div>';
        html += '</li>';

        return html;
    }

    function scroll_chat_window() {
        $('.chat').slimScroll({alwaysVisible: true, scrollTo: $("#last_messages").height() + $("#new_messages").height() + 'px' });
    }

    ChatApi.start_chat_ws = function(thread_url) {
        console.log(thread_url);
        ChatApi.ws = new WebSocket(thread_url);
        ChatApi.ws.onopen = function() {
            $("#last_messages").html('');
            $("#new_messages").html('');
        };
        ChatApi.ws.onmessage = function(event) {
            var messages_data = JSON.parse(event.data);
            for (var i = 0; i < messages_data.length; i++) {
                message_data = messages_data[i]
                send_html = get_message_html(message_data.message_id, message_data.sender, message_data.user_id, message_data.timestamp, message_data.text, message_data.avatar, message_data.image)
                if (message_data.type == 'last') {
                    $("#last_messages").prepend(send_html);
                } else {
                    $("#new_messages").append(send_html);
                }

            }
            scroll_chat_window();
        };
        ChatApi.ws.onclose = function(){
            setTimeout(function() {ChatApi.start_chat_ws(thread_url)}, 1000);
        };
    };


    function send_message() {
        var textarea = $(".message-js");
        if (textarea.val() == "") {
            return false;
        }
        if (ChatApi.ws.readyState != WebSocket.OPEN) {
            return false;
        }
        image = null;
        if ($('.attach-image-js').css('display') != 'none') {
            image = {full: $('.attach-a-js').attr('href'), thumb: $('.attach-a-js > img').attr('src'), id: $('.attach-a-js').attr('image-id')};
        }

        message = JSON.stringify({message: textarea.val(), image: image});
        ChatApi.ws.send(message);
        textarea.val("");
        $('.attach-image-js').hide();
    };

    $('.attach-js').click(function(){
        $('.upload-image-js').click();
    });

    $('.hide-attach-js').click(function(){
        $('.attach-image-js').hide();
    });

    $('.smile-js').click(function(){
        if ($('.smile').css('display') == 'none') {
            $('.smile').show();
        } else {
            $('.smile').hide();
        }
    });


    $('.upload-image-js').change(function(){
        var formData = new FormData($('form')[0]);
        formData.append("api_key", ChatApi.api_key);
        $.ajax({
            url: '/apichat/upload',
            type: 'POST',
            data: formData,
            cache: false,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function(data){
                if (data.status == 'ok') {
                    $('.attach-a-js > img').attr('src', data.image_thumb);
                    $('.attach-a-js').attr('href', data.image_big);
                    $('.attach-a-js').attr('image-id', data.id);
                    $('.attach-image-js').show();
                } else if (data.status == 'error') {
                    alert(data.error);
                }
            },
            error: function(data) {
                alert(data.statusText);
            }
        });
    });

});







