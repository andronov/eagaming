$( document ).ready(function() {
    /*var csrftoken = $.cookie('csrftoken');
    console.log(csrftoken);
    $.ajaxSetup({
       data: {'csrfmiddlewaretoken': csrftoken}
    });*/
    ChatApi.getThread('uuu', 'ru', function(event) {
        if ("WebSocket" in window) {
            thread_id = event.thread_id;

            ChatApi.start_chat_ws(event.thread_url);
        } else {
            $(".chat").html('<div class="outdated_browser_message"><p><em>Ой!</em> Вы используете устаревший браузер. Пожалуйста, установите любой из современных:</p><ul><li>Для <em>Android</em>: <a href="http://www.mozilla.org/ru/mobile/">Firefox</a>, <a href="http://www.google.com/intl/en/chrome/browser/mobile/android.html">Google Chrome</a>, <a href="https://play.google.com/store/apps/details?id=com.opera.browser">Opera Mobile</a></li><li>Для <em>Linux</em>, <em>Mac OS X</em> и <em>Windows</em>: <a href="http://www.mozilla.org/ru/firefox/fx/">Firefox</a>, <a href="https://www.google.com/intl/ru/chrome/browser/">Google Chrome</a>, <a href="http://ru.opera.com/browser/download/">Opera</a></li></ul></div>');
            return false;
        }
    });

});