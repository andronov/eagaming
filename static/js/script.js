function RefreshTime () {
    function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
    }
    function get_cookie(cookie_name) {
              var results = document.cookie.match('(^|;) ?' + cookie_name + '=([^;]*)(;|$)');
              if (results)
                  return ( unescape(results[2]) );
              else
                  return null;
    }
    //чат скрывает отображает
    if (get_cookie('chat') == 'close'){
        $(".body-fixed-chat").hide();
        $('.header-fixed-chat span').removeClass('glyphicon-collapse-down').addClass('glyphicon-collapse-up');
    }
    else{
        $(".body-fixed-chat").show();
        $('.header-fixed-chat span').removeClass('glyphicon-collapse-up').addClass('glyphicon-collapse-down');

    }
    $(".fixed-click").click(function () {
        //createCookie('chat','close',2);
        if ($('.header-fixed-chat span').hasClass('glyphicon-collapse-down')){
            createCookie('chat','close',2);
            $('.header-fixed-chat span').removeClass('glyphicon-collapse-down').addClass('glyphicon-collapse-up');
        }
        else if ($('.header-fixed-chat span').hasClass('glyphicon-collapse-up')){
            createCookie('chat','open',2);
            $('.header-fixed-chat span').removeClass('glyphicon-collapse-up').addClass('glyphicon-collapse-down');
        }
      $(".body-fixed-chat").slideToggle("slow");
    });

    var times = new Date(get_cookie('time').replace(/\\054/g, ','));
    var today = new Date();
    if (today.getDate() == times.getDate()){
        $('.time-button').removeClass('active');
        $("#time-yesterday").toggleClass("active");
    }
    else if(today.getDate() + 1 == times.getDate()){
        $('.time-button').removeClass('active');
        $("#time-today").toggleClass("active");
    }
    else if(today.getDate() + 2 == times.getDate()){
        $('.time-button').removeClass('active');
        $("#time-tomorrow").toggleClass("active");
    }
    else{
        $('.time-button').removeClass('active');
        $("#time-all").toggleClass("active");
    }

    var desc = get_cookie('discip');
    //$('.discipline-a').toggleClass('active');
    $('.discipline-a').each(function() {
        if(desc.indexOf($(this).attr('data-value'))!== -1) {
           // $(this).toggleClass('active');
            console.log($("items-desc-"+$(this).attr('data-value')+""));

            if ($(".items-desc-" + $(this).attr('data-value'))) {
                $(".items-desc-"+$(this).attr('data-value')+"").remove();
            }


            $(this).addClass('active');
            var src = "<img class='discipline-img' src='" + $(this + ' .discipline-img').attr('src') + "'> " + $(this).text();
            $(".dropdown-desct").prepend("<div  class='item-desc items-desc-" + $(this).attr('data-value') + "'>" + $(this).html() + "</div>");
        }
        else{
            console.log($(this).hasClass('active'));
            if (!$(this).hasClass('active')) {
                $(".items-desc-"+$(this).attr('data-value')+"").remove();
            }
            else{

            }
            $(this).removeClass('active');

        }
     });

    /*
    $('#discipline-a').each(function(i,elem) {
        console.log($(this));
	     if ($(this).hasClass("stop")) {
		    console.log('555');
	     } else {
             console.log('777');
	     }
    });
    */
}


var actionButtonsCallbacks = function () {
  return {
    click: {
      //Default click callback
      defaultCallback: function(e, trigger) {
        alert('default');
      }
    },
    submit: {
      //Default submit callback
      defaultCallback: function(e, form, data) {
        var trigger = form.find('.submit-trigger');
        trigger.on('hidden.bs.tooltip', function () { trigger.tooltip('destroy'); });
        trigger.tooltip({title: 'Сохранено', trigger: 'manual', html: true, placement: 'left'}).tooltip('show');
        setTimeout(function(){ trigger.tooltip('hide'); }, 3000);
      },
      OneAddItemCallback: function(e, form, data) {
        form.find('.alert-danger').remove();
        console.log(1, data);
        if(data['data']['error']) {
            $('.table-responsive').prepend('<div class="alert alert-danger fade in">'+ data['data']['error'] +'<a class="close" data-dismiss="alert" href="#">&times;</a></div>')
        } else {
            $('.table-responsive').prepend('<div class="alert alert-success fade in">'+ data['data']['success'] +'<a class="close" data-dismiss="alert" href="#">&times;</a></div>')
        }
      },
      NameUserEditCallback: function(e, form, data) {
        form.find('.alert-danger').remove();
        console.log(1, data);
        if(data['data']['error']) {
            $('.form-cashout').prepend('<div class="alert alert-danger fade in">'+ data['data']['error'] +'<a class="close" data-dismiss="alert" href="#">&times;</a></div>')
        } else {
            $('.form-cashout').prepend('<div class="alert alert-success fade in">'+ data['data']['success'] +'<a class="close" data-dismiss="alert" href="#">&times;</a></div>')
        }
      },
      FormCashoutCallback: function(e, form, data) {
        form.find('.alert-danger').remove();
        console.log(1, data);
        if(data['data']['error']) {
            $('.form-cashout').prepend('<div class="alert alert-danger fade in">'+ data['data']['error'] +'<a class="close" data-dismiss="alert" href="#">&times;</a></div>')
        } else {
            $('.form-cashout').prepend('<div class="alert alert-success fade in">'+ data['data']['success'] +'<a class="close" data-dismiss="alert" href="#">&times;</a></div>')
        }
      }
      /*
      classSubgroupFormSubmit: function(e, form, data) {
        var replacement = $(data.html);
        if(data.errors) {
          form.replaceWith(replacement);
        } else {
          var id = form.find('input.subgroup_id').val();
          if(parseInt(id)) {
            $('.subgroups-row[data-id="' + id + '"]').replaceWith(replacement);
          } else {
            $('.subgroups-row:last').before(replacement);
          }
          replacement.parents('.div-table-body:first').restripeRows();
          form.parents('.modal').modal('hide');
        }
        $(document).trigger('contentAdded', { target: replacement.parent() });
      }*/
    },
    validate: {
        //Default validate callback. Return TRUE if there is some errors
        defaultCallback: function (e, form, values) {
            return false;
        }
    },
    ajax: {
      //Default AJAX callback
      defaultCallback: function(e, trigger, data) {
        alert('AJAX default');
        console.log(e, trigger, data);
      },
      //
      FilterDateCallback: function(e, trigger, data) {
          RefreshTime();
          var replacement = $(data);
          var row = $(document).find('.line-tbody');
          row.replaceWith(data['data']['html']);
      },
      FilterInputCallback: function(e, trigger, data) {
           //just example!
          RefreshTime();



          var replacement = $(data);
          var row = $(document).find('.line-tbody');
          row.replaceWith(data['data']['html']);

          //$(document).trigger('contentAdded', { target: replacement });
      }
    }
  };
}();

(function($) {

  $.extend($.fn, {
    actionButton: function(){
      $(this).each(function(){
        var trigger = $(this);
        function get_cookie(cookie_name){
         var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
              if ( results )
                  return ( unescape ( results[2] ) );
               else
                   return null;
         }
        if(!trigger.hasClass('action-processed')) {
            var today = new Date();
            var date_ = today.getDate() + 1;
            var date_y = today.getDate() ;
            var date_t = today.getDate() + 2;
            var yr = today.getFullYear();
            var months = (today.getMonth() + 1);
          var dataid = trigger.attr('data-time');
          if(dataid) {
          if(dataid.toString() == 'yesterday'){
              dataid = '' + yr + ',' + months +','+ date_y +''
          }
          else if(dataid.toString() == 'today'){
              dataid = dataid = '' + yr + ',' + months +','+ date_ +''
          }
          else if(dataid.toString() == 'tomorrow'){
              dataid = dataid = '' + yr + ',' + months +','+ date_t +''
          }
          else if(dataid.toString() == 'all'){
              dataid = 'all'
          }}
          //var discipf  = $('#discipline-a').hasClass("active");
            //console.log($("#dropdown-menu-desc").find(".active"));

            //console.log(discipf);
            //var discip  = '1';
          var url = trigger.attr('href') && trigger.attr('href') != '#' ? trigger.attr('href') : trigger.data('href'), callback = actionButtonsCallbacks.click.defaultCallback, callbackText = trigger.data("callback"),
              method = trigger.data('method') ? trigger.data('method').toLowerCase() : 'post',
              ajaxing_cont = trigger.data('ajaxcont') ? $(trigger.data('ajaxcont')) : trigger;
          if(url && url != '#' && !trigger.data('noajax')) {
            //ajaxCallback
            callback = callbackText && $.isFunction(actionButtonsCallbacks.ajax[callbackText]) ? actionButtonsCallbacks.ajax[callbackText] : actionButtonsCallbacks.ajax.defaultCallback;
            var csrf = $("input[name='csrfmiddlewaretoken']").val();
            trigger.click(function(e) {
              ajaxing_cont.addClass('ajaxing');
              ajaxing_cont.toggleClass("active");

              var discip  = [];
              $("a[data-role=desc]").each(function (){
                 if($(this).hasClass("active")){
                     console.log($(this).attr("data-value"));
                     discip.push($(this).attr("data-value"));

                 }
              });
              if(!discip){
                  discip  = [0];
              }
                console.log(777,discip, dataid);

              e.preventDefault();
              $.ajax({
                url: url,
                data: {'csrfmiddlewaretoken':csrf, 'time': dataid, 'discip':discip},
                method:'post',
                success: function(data) {
                  //ajaxing_cont.removeClass('ajaxing').addClass('ajaxingOut');
                  //setTimeout(function(){ ajaxing_cont.removeClass('ajaxingOut'); }, 400);
                  //Trigger success callback
                  callback(e, trigger, data);
                }
              });
            });

          } else {
            //simpleCallback
            callback = callbackText && $.isFunction(actionButtonsCallbacks.click[callbackText]) ? actionButtonsCallbacks.click[callbackText] : actionButtonsCallbacks.click.defaultCallback;
            trigger.click(function(e) {
              e.preventDefault();
              callback(e, trigger);
            });
          }
          trigger.addClass('action-processed');
        }
      });

      return this;
    },
    //Good serialization of form for AJAX
    serializeFormToObject: function(){
      var o = {};
      var a = this.serializeArray();
      $.each(a, function() {
        if (o[this.name] !== undefined) {
          if (!o[this.name].push) {
              o[this.name] = [o[this.name]];
          }
          o[this.name].push(this.value || '');
        } else {
          o[this.name] = this.value || '';
        }
      });
      return o;
    },
    //AJAX Form
    ajaxifyForm: function(){
      $(this).each(function(){
        var form = $(this);
        if(!form.hasClass('ajax-processed')) {
          form.submit(function(e) {
            e.preventDefault();
            var submitCallback = form.data("submit") && $.isFunction(actionButtonsCallbacks.submit[form.data("submit")]) ? actionButtonsCallbacks.submit[form.data("submit")] : actionButtonsCallbacks.submit.defaultCallback,
                validateCallback = form.data("validate") && $.isFunction(actionButtonsCallbacks.validate[form.data("validate")]) ? actionButtonsCallbacks.validate[form.data("validate")] : actionButtonsCallbacks.validate.defaultCallback,
                errors = false, values = form.serializeFormToObject(),
                url = form.attr('action') ?  form.attr('action') : window.location.href,
                method = form.attr('method') ? form.attr('method').toLowerCase() : 'get';
            //Check form for errors
            errors = validateCallback(e, form,  values);
            //If NO ERRORS than SEND form
            if(!errors) {
              $.ajax({
                url: url,
                data: values,
                method: method,
                success: function(data) {
                  //Trigger submit callback
                  submitCallback(e, form, data);
                }
              });
            }
          });
          form.addClass('ajax-processed');
        }
      });

      return this;
    }
  });

  //Custom event for AJAX loaded content
  $(document).on('contentAdded', function(e, data){
    var target = data.target;
    //Action Buttons
    target.find('.action-button').actionButton();
    //AJAX forms
    target.find('.ajaxify-form').ajaxifyForm();
  });



  $(document).ready(function(){
    //Event trigget on DOM ready
    $(this).trigger('contentAdded', {target: $('html')});
  });

    /*var todays = $("#time-today");
    var yester = $("#time-yesterday");
    var tommorow = $("#time-tomorrow");
    function time() {
        var today = new Date();
        var day_of_week = ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"];
        var month_of_year = ["Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября", "Октября", "Ноября", "Декабря"];
        var date_ = today.getDate();
        var date_y = today.getDate() - 1;
        var date_t = today.getDate() + 1;
        var months = (today.getMonth() + 1);
        var yr = today.getFullYear();
        var month_ = month_of_year[today.getMonth()];

        yester.html(date_y+' '+month_);
        todays.html(date_+' '+month_);
        tommorow.html(date_t+' '+month_);
        $('#time-today').attr('data-time','' + yr + ',' + months +','+ date_ +'');
    }
    setInterval(time, 1);
    //-->*/


})(jQuery);